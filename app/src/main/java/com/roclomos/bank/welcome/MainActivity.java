package com.roclomos.bank.welcome;


import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.nio.channels.FileChannel;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class MainActivity extends BaseAct  {

    private static final int SELECT_PICTURE = 1;
    ImageView imgView;// = (ImageView) findViewById(R.id.imageview);
    private String selectedImagePath;
    private static final int CAMERA_REQUEST = 1888;
    Bitmap bmp;
    Uri outputFileUri;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

       // Toast.makeText(MainActivity.this, "Thread.setDefaultUncaughtExceptionHandler(handleAppCrash);start", Toast.LENGTH_SHORT).show();
        Button photoButton = (Button) this.findViewById(R.id.Button02);
        EditText et = (EditText) findViewById(R.id.edName);
        et.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                                        @Override
                                        public void onFocusChange(View view, boolean b) {
                                            if(!b)
                                                hidekeyb(view);
                                        }
                                    }

        );



        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {

            ((Button)findViewById(R.id.Button01)).setVisibility(View.INVISIBLE);
            // only for gingerbread and newer versions
        }


        //asyncTask.delegate=this;
        imgView = (ImageView) findViewById(R.id.imageview);
        photoButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                File root = new File(Environment
                        .getExternalStorageDirectory()
                        + File.separator + "myDir" + File.separator);
                root.mkdirs();
                File  sdImageMainDirectory = new File(root, "welcomeapp.jpg");

                outputFileUri = Uri.fromFile(sdImageMainDirectory);

                Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);

                startActivityForResult(intent, CAMERA_REQUEST);



                //Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                //startActivityForResult(Intent.createChooser(intent, "Select Picture"), CAMERA_REQUEST);

            }
        });
        ((Button) findViewById(R.id.Button01))
                .setOnClickListener(new View.OnClickListener() {

                    public void onClick(View arg0) {

                        // in onCreate or any event where your want the user to
                        // select a file
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.setAction(Intent.ACTION_GET_CONTENT);

                        startActivityForResult(Intent.createChooser(intent,
                                "Select Picture"), SELECT_PICTURE);

                    }
                });
        Button save =(Button)findViewById(R.id.save);
        save.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
if(filepath.equals("")||((EditText)findViewById(R.id.edName)).getText().toString().equals(""))
{
    Toast.makeText(MainActivity.this, "Please enter Name and Choose an Image", Toast.LENGTH_SHORT).show();
return;
}
            //    ConnectServer client = new ConnectServer();
                String id = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
                RequestQueue queue = Volley.newRequestQueue(MainActivity.this);
                String pname =((EditText)findViewById(R.id.edName)).getText().toString().replace(" ","lmpno");
                String url ="http://demo.roclomos.com/welcome/link.php?name="+pname+"&id="+id;
// Request a string response from the provided URL.
                StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // Display the first 500 characters of the response string.
                                //mTextView.setText("Response is: "+ response.substring(0,500));
                         //       Toast.makeText(MainActivity.this, response, Toast.LENGTH_SHORT).show();
                                Thread tred = new Thread(new Runnable() {

                                    @Override
                                    public void run() {
                                        // TODO Auto-generated method stub
                                        FtpfileUpload ftpf = new FtpfileUpload();
                                        String id = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
                                        ftpf.uploadfile(filepath, id);
                                        Intent intent=new Intent(MainActivity.this, MonitorService.class);
                                        intent.putExtra("id",id);
                                        startService(intent);
                                    //   MainActivity.this.finish();
                                    }
                                });
tred.start();
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(MainActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(Intent.ACTION_SENDTO); // it's not ACTION_SEND
                        intent.setType("text/plain");
                        intent.putExtra(Intent.EXTRA_SUBJECT, "Save Button");
                        intent.putExtra(Intent.EXTRA_TEXT,error.getMessage());
                        intent.setData(Uri.parse("mailto:muruganandam8817@gmail.com")); // or just "mailto:" for blank
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // this will make such that when user returns to your app, your app is displayed, instead of the email app.
                        startActivity(intent);

                        //mTextView.setText("That didn't work!");
                    }
                });
// Add the request to the RequestQueue.
                queue.add(stringRequest);


            }
        });
    }
    private String readStream(InputStream in) {
        BufferedReader reader = null;
        StringBuffer response = new StringBuffer();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return response.toString();
    }



    public static Uri handleImageUri(Uri uri) {
        Pattern pattern = Pattern.compile("(content://media/.*\\d)");
        if (uri.getPath().contains("content")) {
            Matcher matcher = pattern.matcher(uri.getPath());
            if (matcher.find())
                return Uri.parse(matcher.group(1));
            else
                throw new IllegalArgumentException("Cannot handle this URI");
        } else
            return uri;
    }



    static String er;
    Intent ret;
    Uri selectedImage;
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        ret=data;
        if (resultCode == RESULT_OK ) {
            if (requestCode == SELECT_PICTURE) {
             //   Bundle b= data.getExtras();
                // for(String s:  b.keySet())

              //  Toast.makeText(MainActivity.this, data.getExtras().get("dat").toString(), Toast.LENGTH_SHORT).show();
                // selectedImage = data.getData();
             /*   check*/
              //  Toast.makeText(MainActivity.this, data.toString(), Toast.LENGTH_SHORT).show();
                String imgDecodableString="";
                Cursor cursor = null;
                try {
                    Uri newUri = handleImageUri(data.getData());
                    String[] proj = { MediaStore.Images.Media.DATA };
                    cursor = MainActivity.this.getContentResolver().query(newUri,  proj, null, null, null);
                    int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                    cursor.moveToFirst();
                    imgDecodableString =cursor.getString(column_index);
                } catch (Exception e){
                   // return null;
                } finally {
                    if (cursor != null) {
                        cursor.close();
                    }
                }





                /* commented at 8
                String[] filePathColumn = { MediaStore.Images.Media.DATA };

                // Get the cursor
                Cursor cursor = getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                // Move to first row
                cursor.moveToFirst();
*/
             //   int columnIndex = cursor.getColumnIndex(columnIndex);

              //  Toast.makeText(MainActivity.this, imgDecodableString, Toast.LENGTH_SHORT).show();




                Integer last = imgDecodableString.lastIndexOf('.');
                String suffix=imgDecodableString.substring(last);
                suffix=suffix.toLowerCase();
                //Toast.makeText(MainActivity.this, suffix, Toast.LENGTH_SHORT).show();
                if(!suffix.equalsIgnoreCase(".jpg")&&!suffix.equalsIgnoreCase(".jpeg"))
                {
                    Toast.makeText(MainActivity.this, "Please Select a JPG file", Toast.LENGTH_SHORT).show();
                    return;
                }
               // Toast.makeText(MainActivity.this,suffix,Toast.LENGTH_LONG).show();
                cursor.close();
                ImageView imgView = (ImageView) findViewById(R.id.imageview);
                // Set the Image in ImageView after decoding the String
               // imgView.setImageBitmap(BitmapFactory
                //        .decodeFile(imgDecodableString));
                Bitmap b = BitmapFactory.decodeFile(imgDecodableString);
                ByteArrayOutputStream out = new ByteArrayOutputStream();
               b.compress(Bitmap.CompressFormat.JPEG, 30, out);

         //       Toast.makeText(MainActivity.this, ""+out.size(), Toast.LENGTH_SHORT).show();
                Bitmap decoded = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));
bmp=Bitmap.createScaledBitmap(b,500,500,true);
             //   Toast.makeText(MainActivity.this, imgDecodableString+"", Toast.LENGTH_SHORT).show();

/*String newname = imgDecodableString.substring(0,imgDecodableString.lastIndexOf('/'))+id+".jpg";
                Toast.makeText(MainActivity.this, newname+"", Toast.LENGTH_SHORT).show();
                    File from = new File(imgDecodableString);
                    File to = new File(newname);
                    if(from.exists()) {
                     if(   from.renameTo(to))
                     {
                         Toast.makeText(MainActivity.this, "success", Toast.LENGTH_SHORT).show();
                     }
                    }*/
                imgView.setImageBitmap(bmp);
                filepath=imgDecodableString;
                // selectedImagePath = getPath(selectedImageUri);


            }
            else  if (requestCode == CAMERA_REQUEST  ) {
                Bitmap  bitmap;
             //   Toast.makeText(MainActivity.this, "test", Toast.LENGTH_SHORT).show();
new CountDownTimer(2000,1000) {

    @Override
    public void onTick(long l) {

    }

    @Override
    public void onFinish() {
Bitmap bmp = BitmapFactory.decodeFile(outputFileUri.getPath());

        File f = new File(outputFileUri.getPath());
        Bitmap bbb=null;
        try {
            FileOutputStream fos = new FileOutputStream(f);
            bbb = bmp.createScaledBitmap(bmp,((int) Math.round((float)bmp.getWidth()*0.75)),((int) Math.round((float)bmp.getHeight()*0.75)),false);
            bbb.compress(Bitmap.CompressFormat.JPEG, 80, fos);

            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            Toast.makeText(MainActivity.this, "err", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        } catch (IOException e) {
            Toast.makeText(MainActivity.this, "err", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        //bmp.compress(Bitmap.CompressFormat.JPEG, 40, out);
//                        Toast.makeText(MainActivity.this, ""+out.size(), Toast.LENGTH_SHORT).show();
        //Bitmap decoded = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));
        //filepath=outputFileUri.getPath();
      //Bitmap  bmp1=Bitmap.createScaledBitmap(bmp,1000,1000,true);




        MainActivity.this.imgView=(ImageView)

                findViewById(R.id.imageview);

        MainActivity.this.imgView.setImageBitmap(bbb);
        filepath=outputFileUri.getPath();
    }
}.start();

         /*       if(data.getData()==null){
                  bitmap = (Bitmap)data.getExtras().get("data");
                    ByteArrayOutputStream out = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 70, out);
//                        Toast.makeText(MainActivity.this, ""+out.size(), Toast.LENGTH_SHORT).show();
                    Bitmap decoded = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));
                    //   filepath=imgDecodableString;
                    bmp=Bitmap.createScaledBitmap(bitmap,500,500,true);
                    MainActivity.this.imgView.setImageBitmap(bmp);
                    //imgView=(ImageView)findViewById(R.id.imageview);
                    //imgView.setImageBitmap(bitmap);
                }else{
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), data.getData());

                        ByteArrayOutputStream out = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, out);
//                        Toast.makeText(MainActivity.this, ""+out.size(), Toast.LENGTH_SHORT).show();
                        Bitmap decoded = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));
                     //   filepath=imgDecodableString;
                        bmp=Bitmap.createScaledBitmap(bitmap,500,500,true);
                        MainActivity.this.imgView.setImageBitmap(bmp);


                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
if(data.getExtras().get("data")!=null) {
    Toast.makeText(MainActivity.this, data.getExtras().get("data").toString(), Toast.LENGTH_SHORT).show();
    return;
}               //comm
         selectedImage = data.getData();

                Bitmap photo = (Bitmap) data.getExtras().get("data");
                imgView.setImageBitmap(photo);

                        //Toast.makeText(MainActivity.this, ""+selectedImage, Toast.LENGTH_SHORT).show();
                        String[] filePathColumn = { MediaStore.Images.Media.DATA };

                        // Get the cursor
                        Cursor cursor = getContentResolver().query(selectedImage,
                                filePathColumn, null, null, null);
                        // Move to first row
                        cursor.moveToFirst();

                        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                        String imgDecodableString = cursor.getString(columnIndex);
                        cursor.close();

                        // Set the Image in ImageView after decoding the String
                        // imgView.setImageBitmap(BitmapFactory
                        //        .decodeFile(imgDecodableString));
                        Bitmap b = BitmapFactory.decodeFile(imgDecodableString);
                File f = new File(imgDecodableString);
                try {
                    FileOutputStream fos = new FileOutputStream(f);
                    Bitmap bbb = b.createScaledBitmap(b,500,500,false);
                                       bbb.compress(Bitmap.CompressFormat.JPEG, 85, fos);

                    fos.flush();
                    fos.close();
                } catch (FileNotFoundException e) {
                    Toast.makeText(MainActivity.this, "err", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                } catch (IOException e) {
                    Toast.makeText(MainActivity.this, "err", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                        b.compress(Bitmap.CompressFormat.JPEG, 70, out);
//                        Toast.makeText(MainActivity.this, ""+out.size(), Toast.LENGTH_SHORT).show();
                        Bitmap decoded = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));
filepath=imgDecodableString;
                        bmp=Bitmap.createScaledBitmap(b,500,500,true);
                        MainActivity.this.imgView.setImageBitmap(bmp); // selectedImagePath = getPath(selectedImageUri);
  //                      Toast.makeText(MainActivity.this, imgDecodableString, Toast.LENGTH_SHORT).show();
          */      //                imageView.setImageBitmap(photo);
            }
        }
    }

    public void hidekeyb(View v)
    {
        InputMethodManager imm=(InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(),0);
    }


String filepath;
    @Override
    protected void onPause()
    {
        super.onPause();

    }

    @Override
    protected  void onResume()
    {
        super.onResume();
        imgView = (ImageView) findViewById(R.id.imageview);
        imgView.setImageBitmap(bmp);
    }
    public  boolean copyFile(Uri from, String to) {
        try {
            File sd = Environment.getExternalStorageDirectory();
            if (sd.canWrite()) {
                File f = new File(new URI(from.toString()));
           //     Toast.makeText(MainActivity.this, f.getAbsolutePath(), Toast.LENGTH_SHORT).show();

                int end = from.toString().lastIndexOf("/");
                String str1 = from.toString().substring(0, end);
                String str2 = from.toString().substring(end+1, from.toString().length());
                File source = new File(str1, str2);
                File destination= new File(to, str2);
                if (source.exists()) {
                    FileChannel src = new FileInputStream(source).getChannel();
                    FileChannel dst = new FileOutputStream(destination).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                }
            }
            return true;
        } catch (Exception e) {
            er=e.getMessage().toString();
            //Toast.makeText(, ""+Environment.getExternalStorageDirectory().toString()+"/asp.jpg", Toast.LENGTH_SHORT).show();
            return false;
        }
    }


    public String getPath(Uri uri) {
        // just some safety built in
        if( uri == null ) {
            // TODO perform some logging or show user feedback
            return null;
        }
        // try to retrieve the image from the media store first
        // this will only work for images selected from gallery
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor =null;// ma(uri, projection, null, null, null);
        if( cursor != null ){
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
        // this is our fallback here
        return uri.getPath();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
