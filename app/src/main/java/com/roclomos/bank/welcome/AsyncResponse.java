package com.roclomos.bank.welcome;

/**
 * Created by True Colors on 30-09-2015.
 */
public interface AsyncResponse {
        void processFinish(String output);
}