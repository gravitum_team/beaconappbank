package com.roclomos.bank.welcome;
/**
 * Created by True Colors on 01-10-2015.
 */
        import java.util.List;
        import java.util.concurrent.TimeUnit;

        import com.android.volley.Request;
        import com.android.volley.RequestQueue;
        import com.android.volley.Response;
        import com.android.volley.VolleyError;
        import com.android.volley.toolbox.StringRequest;
        import com.android.volley.toolbox.Volley;
        import com.estimote.sdk.BeaconManager;
        import com.estimote.sdk.Region;

        import java.util.List;
        import java.util.concurrent.TimeUnit;

        import android.annotation.SuppressLint;
        import android.app.Activity;
        import android.app.ActivityManager;
        import android.app.ActivityManager.RunningTaskInfo;
        import android.app.Notification;
        import android.app.NotificationManager;
        import android.app.PendingIntent;
        import android.app.Service;
        import android.bluetooth.BluetoothAdapter;
        import android.content.Context;
        import android.content.Intent;
        import android.os.Bundle;
        import android.os.IBinder;
        import android.os.RemoteException;
        import android.provider.Settings;
        import android.support.v4.app.NotificationCompat;
        import android.util.Log;
        import android.view.Menu;
        import android.widget.EditText;
        import android.widget.Toast;


        import com.estimote.sdk.Beacon;
        import com.estimote.sdk.BeaconManager;
        import com.estimote.sdk.BeaconManager.MonitoringListener;
        import com.estimote.sdk.Region;
        import com.estimote.sdk.Utils;
public class MonitorService extends Service {
    static boolean entered=false;
    private static final String TAG = "MyService";
    //MediaPlayer player;
    private static final String ESTIMOTE_PROXIMITY_UUID = "B9407F30-F5F8-466E-AFF9-25556B57FE6D";
    private static final Region ALL_ESTIMOTE_BEACONS = new Region("regionId", null, null, null);
    private static final int NOTIFICATION_ID = 123;
    NotificationManager notificationManager;
    private BeaconManager beaconManager;
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    protected void onStop() {

        try {
            beaconManager.stopRanging(ALL_ESTIMOTE_BEACONS);

        } catch (RemoteException e) {
            //   Log.e(TAG, "Cannot stop but it does not matter now", e);
        }
        // super.onStop();
    }
    public static boolean isAppInForeground(
            Context context) {
        List<RunningTaskInfo> task = ((ActivityManager)
                context.getSystemService(
                        Context.ACTIVITY_SERVICE))
                .getRunningTasks(1);
        if (task.isEmpty()) {
            return false;
        }
        return task
                .get(0)
                .topActivity
                .getPackageName()
                .equalsIgnoreCase(
                        context.getPackageName());
    }
    @Override
    public void onCreate() {

        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (!mBluetoothAdapter.isEnabled()) {
            mBluetoothAdapter.enable();
        }
        beaconManager= new BeaconManager(getBaseContext());
        notificationManager = (NotificationManager)
                getSystemService(
                        Context.NOTIFICATION_SERVICE);
        beaconManager.setBackgroundScanPeriod(
                TimeUnit.SECONDS.toMillis(1), 0);
        //Toast.makeText(this, "My Service Created", Toast.LENGTH_LONG).show();
        //Log.d(TAG, "onCreate");
        //Utils.computeAccuracy(beacon)
        //player = MediaPlayer.create(this, R.raw.braincandy);
        //	player.setLooping(false); // Set looping
        beaconManager.setMonitoringListener(new MonitoringListener() {

            @Override
            public void onExitedRegion(Region arg0) {
                // TODO Auto-generated method stub
                entered = false;
                Toast.makeText(
                        getApplicationContext(),
                        "Exited region",
                        Toast.LENGTH_LONG).show();

		             /*       try {
		                        beaconManager.stopRanging(
		                                    ALL_ESTIMOTE_BEACONS);
		                    } catch (RemoteException e) {
		                        Log.e(TAG,
		                            "Cannot start ranging", e);
		                    }
			*/
            }

            @Override
            public void onEnteredRegion(Region arg0, List<Beacon> arg1) {
                // TODO Auto-generated method stub

                for (Beacon b : arg1) {
                    if (!entered) {
                        entered = true;
                        if (Utils.computeProximity(b) == Utils.Proximity.NEAR || Utils.computeProximity(b) == Utils.Proximity.IMMEDIATE) {
                            RequestQueue queue = Volley.newRequestQueue(MonitorService.this);
                            String url = "http://demo.roclomos.com/welcome/detected.php?id=" + id;
                            // Request a string response from the provided URL.
                            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                                    new Response.Listener<String>() {
                                        @Override
                                        public void onResponse(String response) {
                                            // Display the first 500 characters of the response string.
                                            //mTextView.setText("Response is: "+ response.substring(0,500));
                                            //         Toast.makeText(MainActivity.this, response, Toast.LENGTH_SHORT).show();

                                        }
                                    }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    //  Toast.makeText(MainActivity.this, "its not working", Toast.LENGTH_SHORT).show();
                                    //mTextView.setText("That didn't work!");
                                    Toast.makeText(

                                            getApplicationContext(),

                                            "Detected a Beacon with Proximity=Near",
                                            Toast.LENGTH_LONG).

                                            show();

                                }
                            });

                            queue.add(stringRequest);

                            break;
                        }

                    }

                }


			     /*               try {
			                        beaconManager.startRanging(
			                                    ALL_ESTIMOTE_BEACONS);
			                    } catch (RemoteException e) {
			                        Log.e(TAG,
			                            "Cannot start ranging", e);
			                    }
			       */
        }
    });






        beaconManager.setRangingListener(new BeaconManager.RangingListener() {

            @Override
            public void onBeaconsDiscovered(Region region, List<Beacon> beacons) {
                String be = "";
                // List<Integer> dist = new ArrayList<Integer>();
                int count = 0;
                double dist = 0;
                Beacon closest = null;
                for (Beacon beacon : beacons) {
                    if (count == 0) {
                        dist = Utils.computeAccuracy(beacon);
                        count = 1;
                        closest = beacon;
                    } else {
                        if (dist > Utils.computeAccuracy(beacon))
                            dist = Utils.computeAccuracy(beacon);
                        closest = beacon;
                    }

                    be += "id:" + beacon.getName();
                }
                Toast.makeText(getApplicationContext(), "Beacons found:" + be, Toast.LENGTH_SHORT).show();
                if (closest != null)
                    Toast.makeText(getApplicationContext(), "Closest Beacon:" + closest.getName(), Toast.LENGTH_SHORT).show();


                //  	 Log.d(TAG, "Ranged beacons: " + beacons);
            }
        });
    }

    @Override
    public void onDestroy() {
        beaconManager.disconnect();
        notificationManager.cancel(NOTIFICATION_ID);
        //	Toast.makeText(this, "My Service Stopped", Toast.LENGTH_LONG).show();
        //Log.d(TAG, "onDestroy");
        //player.stop();
    }

    private void postNotification(String msg) {

    }

String id="";
    @Override
    public void onStart(Intent intent, int startid) {
id=intent.getStringExtra("id");
             //   Toast.makeText(this,,Toast.LENGTH_LONG).show();
        notificationManager.cancel(NOTIFICATION_ID);
        beaconManager.connect(new
                                      BeaconManager.ServiceReadyCallback() {
                                          @Override
                                          public void onServiceReady() {
                                              try {
                                                  beaconManager.startMonitoring(
                                                          ALL_ESTIMOTE_BEACONS);
                                              } catch (RemoteException e) {
                                                  Log.d(TAG,
                                                          "Error while starting monitoring");
                                              }
                                          }
                                      });
        //	Toast.makeText(this, "My Service Started", Toast.LENGTH_LONG).show();
        //Log.d(TAG, "onStart");
        //player.start();
    }
}
